from keras.models import Sequential
from keras.layers import Dense
from numpy import array
from numpy.random import uniform
from numpy import hstack
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import pandas as pd
import collections
import random
from tsfresh import extract_features
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers.convolutional import Conv3D
from keras.layers.convolutional_recurrent import ConvLSTM2D
from keras.layers.normalization import BatchNormalization
from keras.regularizers import l2
from pyxlsb import open_workbook as open_xlsb
import keras
import tensorflow as tf
import numpy as np
import xgboost
from collections import OrderedDict




def create_data(n):
    x1 = array([i / 100 + uniform(-1, 3) for i in range(n)]).reshape(n, 1)
    x2 = array([i / 100 + uniform(-3, 5) + 2 for i in range(n)]).reshape(n, 1)
    x3 = array([i / 100 + uniform(-6, 5) - 3 for i in range(n)]).reshape(n, 1)

    y1 = [x1[i] - x2[i] + x3[i] + uniform(-2, 2) for i in range(n)]
    y2 = [x1[i] + x2[i] - x3[i] + 5 + uniform(-1, 3) for i in range(n)]
    X = hstack((x1, x2, x3))
    Y = hstack((y1, y2))
    return X, Y

def readinput():
    df_target = []
    df_parameters = []
    df_simulation_output_target1 = []
    df_simulation_output_target2 = []
    # Extract data from Excel Binary Workbook file
    with open_xlsb('./1000_Case_27092019.xlsb') as wb:
        # Experimental target values 1 - 10 in columns 2 - 11 with frequency in the
        # first column
        with wb.get_sheet(1) as sheet:
            for row in sheet.rows():
                df_target.append([item.v for item in row])

        # Parameter values K1 - K13 in columns 2 - 14 for the different cases in
        # the first column
        with wb.get_sheet(2) as sheet:
            for row in sheet.rows():
                df_parameters.append([item.v for item in row])

        # Simulated output for target 1 for the different cases for a given
        # frequency
        with wb.get_sheet(3) as sheet:
            for row in sheet.rows():
                df_simulation_output_target1.append([item.v for item in row])

        # Simulated output for target 2 for the different cases for a given
        # frequency
        with wb.get_sheet(4) as sheet:
            for row in sheet.rows():
                df_simulation_output_target2.append([item.v for item in row])

    df_target = pd.DataFrame(df_target[1:], columns=df_target[0])
    df_parameters = pd.DataFrame(df_parameters[1:], columns=df_parameters[0])

    df_simulation_output_target1 = pd.DataFrame(df_simulation_output_target1[1:],
                                                columns=df_simulation_output_target1[0])
    df_simulation_output_target1 = df_simulation_output_target1.dropna(how='all')  # Remove empty rows with nans

    df_simulation_output_target2 = pd.DataFrame(df_simulation_output_target2[1:],
                                                columns=df_simulation_output_target2[0])
    df_simulation_output_target2 = df_simulation_output_target2.dropna(how='all')  # Remove empty rows with nans

    # The simulated peak locations are the input
    X_1 = df_simulation_output_target1.iloc[:, 1:].transpose()
    X_2 = df_simulation_output_target2.iloc[:, 1:].transpose()
    X_temp = [X_1, X_2]
    X = pd.concat(X_temp)

    # The parameters are the output
    Y_1 = df_parameters.iloc[:, 1:]
    Y_2 = df_parameters.iloc[:, 1:]
    Y_temp = [Y_1, Y_2]
    Y = pd.concat(Y_temp)

    train_signals, test_signals, Y_train, Y_test = train_test_split(X, Y, test_size=0.33, random_state=4)

    return X, Y, df_target

def main(index):
    X, Y = create_data(n=450)

    # Xi, Yi, df_target = readinput()
    # X, Y, df_target = readinput()
    X = X.transpose()
    Xi, Yi, df_target = readinput()
    Xi = pd.read_csv("extracted_1121_part.csv")
    Xi = Xi.iloc[:,2:-1]
    X = Xi.to_numpy()
    features_filtered = pd.read_csv("filtered feature_K1.csv")

    features_filtered = features_filtered.iloc[:,1:]
    featureColumns = features_filtered.columns
    X = features_filtered.to_numpy()

    scaler = StandardScaler()

    Y = Yi.to_numpy()
    # Y= np.expand_dims(Y,axis=0)
    Y = scaler.fit_transform(Y)





    # for i in range(Y.shape[1]):
    #     Y.iloc[:,i].to_numpy()
        # features_filtered = select_features(extracted_features, Yi.iloc[:,i].to_numpy())
        # features_filtered.to_csv("filtered feature_K"+str(i+1)+".csv")

    # plt.plot(Y)
    # plt.show()

    Yi = Yi.iloc[:, 0]
    xtrain, xtest, ytrain, ytest = train_test_split(X, Y, test_size=0.15)
    ytrain = pd.DataFrame(ytrain).iloc[:, index].to_numpy()
    ytest = pd.DataFrame(ytest).iloc[:, index].to_numpy()



    print("xtrain:", xtrain.shape, "ytrian:", ytrain.shape)

    Xtarget = pd.read_csv("Target_features1000.csv")
    # Xtarget = Xtarget.iloc[:,2:-1]
    headerArr = []
    for i in featureColumns :
        if(i == "Unnamed: 0.1"):
            headerArr.append("Unnamed: 0")
        else:
            headerArr.append(i.replace("Case 1000","Target 10"))
    headerArr1= []
    # for i in Xtarget.columns :
    #     if i in headerArr:
    #         headerArr1.append(i)
    Xtarget = Xtarget[headerArr]

    # Xtarget = Xtarget.iloc[:,2:-1]
    Xtarget = Xtarget.to_numpy()
    print("X:", X.shape, "Y:", Y.shape)
    in_dim = X.shape[1]
    out_dim = Y.shape[1]
    out_dim = 1

    # model = Sequential()
    # model.add(Dense(100, input_dim=in_dim, activation="relu"))
    # model.add(Dense(32, activation="relu"))
    # model.add(Dense(out_dim))
    # model.compile(loss="mse", optimizer="adam")
    # model.summary()
    # model.fit(xtrain, ytrain, epochs=1000, batch_size=10, verbose=2)

    model = xgboost.XGBRegressor(colsample_bytree=0.4,
                                 gamma=0,
                                 learning_rate=0.07,
                                 max_depth=5,
                                 min_child_weight=1.5,
                                 n_estimators=10000,
                                 reg_alpha=0.75,
                                 reg_lambda=0.45,
                                 subsample=0.6,
                                 seed=42)

    # every_column_except_y = [col for col in xtrain.columns ]
    model.fit(xtrain, ytrain)

    print(index+1)
    print("train accuracy",model.score(xtrain, ytrain))
    print("testing accuracy", model.score(xtest, ytest))

    preds =model.predict(xtest)
    rmse = np.sqrt(mean_squared_error(ytest, preds))
    print("RMSE: %f" % (rmse))
    # OrderedDict(sorted(model.booster().get_fscore().items(), key=lambda t: t[1], reverse=True))

    # with tf.device('/gpu:0'):
    #     model = Sequential()
    #     model.add(Dense(500, input_dim=in_dim, activation="relu",kernel_regularizer=l2(0.001)))
    #     model.add(Dense(200, activation="relu", kernel_regularizer=l2(0.001)))
    #     model.add(Dense(100, activation="relu",kernel_regularizer=l2(0.001)))
    #     model.add(Dense(50, activation="relu",kernel_regularizer=l2(0.001)))
    #     model.add(Dense(out_dim))
    #     model.compile(loss="mse", optimizer="adam",metrics=["mean_squared_error"])
    #     model.summary()
    #     model.fit(xtrain, ytrain, epochs=500, batch_size=20, verbose=2,shuffle=True)
    #
    #     # model = Sequential()
    #     # model.add(Dense(500, input_dim=4, activation="relu"))
    #     # model.add(Dense(100, activation="relu"))
    #     # model.add(Dense(50, activation="relu"))
    #     # model.add(Dense(1))
    #     #
    #     # model.compile(loss="mean_squared_error", optimizer="adam", metrics=["mean_squared_error"])
    #     # model.fit(xtrain, ytrain, epochs=200)
    #
    #     ypred = model.predict(xtest)
    #     # print("y1 MSE:%.4f" % mean_squared_error(ytest[:, 0], ypred[:, 0]))
    #     # print("y2 MSE:%.4f" % mean_squared_error(ytest[:, 1], ypred[:, 1]))
    #
    #
    #     ypred1 = model.predict(X)
    #     ypred1 = scaler.inverse_transform(np.concatenate((ypred1,np.zeros(shape=(2000,12))),axis=1))
    #     pd.DataFrame(ypred1).to_csv("predicted_X.csv")
    #
    #
    #     ypred1 = model.predict(Xtarget)
    #     ypred1 = scaler.inverse_transform(np.concatenate((ypred1,np.zeros(shape=(11,12))),axis=1))
    #     pd.DataFrame(ypred1).to_csv("predicted_X1_27_03.csv")
    #
    #     # serialize model to JSON
    #     model_json = model.to_json()
    #     with open("model.json", "w") as json_file:
    #         json_file.write(model_json)
    #     # serialize weights to HDF5
    #     model.save_weights("model.h5")
    #     print("Saved model to disk")

    # x_ax = range(len(xtest))
    # plt.scatter(x_ax, ytest[:, 0], s=6, label="y1-test")
    # plt.plot(x_ax, ypred[:, 0], label="y1-pred")
    # plt.scatter(x_ax, ytest[:, 1], s=6, label="y2-test")
    # plt.plot(x_ax, ypred[:, 1], label="y2-pred")
    # plt.legend()
    # plt.show()


if __name__ == '__main__':
    config = tf.ConfigProto(device_count={'GPU': 1, 'CPU': 56})
    sess = tf.Session(config=config)
    keras.backend.set_session(sess)

    for i in range(13):
        main(i)