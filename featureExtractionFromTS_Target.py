# Common imports
import numpy as np
import os
import matplotlib
import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import collections
import random
from tsfresh import extract_features

from pyxlsb import open_workbook as open_xlsb
from sklearn.model_selection import train_test_split


def readinput():
	df_target = []
	df_parameters = []
	df_simulation_output_target1 = []
	df_simulation_output_target2 = []
	# Extract data from Excel Binary Workbook file
	with open_xlsb('./1000_Case_27092019.xlsb') as wb:
		# Experimental target values 1 - 10 in columns 2 - 11 with frequency in the
		# first column
		with wb.get_sheet(1) as sheet:
			for row in sheet.rows():
				df_target.append([item.v for item in row])

		# Parameter values K1 - K13 in columns 2 - 14 for the different cases in
		# the first column
		with wb.get_sheet(2) as sheet:
			for row in sheet.rows():
				df_parameters.append([item.v for item in row])

		# Simulated output for target 1 for the different cases for a given
		# frequency
		with wb.get_sheet(3) as sheet:
			for row in sheet.rows():
				df_simulation_output_target1.append([item.v for item in row])

			# Simulated output for target 2 for the different cases for a given
		# frequency
		with wb.get_sheet(4) as sheet:
			for row in sheet.rows():
				df_simulation_output_target2.append([item.v for item in row])

	df_target = pd.DataFrame(df_target[1:], columns=df_target[0])
	df_parameters = pd.DataFrame(df_parameters[1:], columns=df_parameters[0])

	df_simulation_output_target1 = pd.DataFrame(df_simulation_output_target1[1:],
												columns=df_simulation_output_target1[0])
	df_simulation_output_target1 = df_simulation_output_target1.dropna(how='all')  # Remove empty rows with nans

	df_simulation_output_target2 = pd.DataFrame(df_simulation_output_target2[1:],
												columns=df_simulation_output_target2[0])
	df_simulation_output_target2 = df_simulation_output_target2.dropna(how='all')  # Remove empty rows with nans

	# The simulated peak locations are the input
	X_1 = df_simulation_output_target1.iloc[:, 1:].transpose()
	X_2 = df_simulation_output_target2.iloc[:, 1:].transpose()
	X_temp = [X_1, X_2]
	X = pd.concat(X_temp)

	# The parameters are the output
	Y_1 = df_parameters.iloc[:, 1:]
	Y_2 = df_parameters.iloc[:, 1:]
	Y_temp = [Y_1, Y_2]
	Y = pd.concat(Y_temp)

	train_signals, test_signals, Y_train, Y_test = train_test_split(X, Y, test_size=0.33, random_state=4)

	return X,Y,df_target

def getNextDataframe(Xi,j):
    Xif1 = Xi.iloc[:,0]
    for i in range( 1,j+100):
        Xif1 = Xif1.append(Xi.iloc[:,i])
    Xif1 = pd.DataFrame(Xif1)
    Xif1["id"] = 1

    for i in range(0,j+100):
        Xif1.iloc[i,1] = int(i/197)
    from tsfresh import extract_features
    extracted_features = extract_features(Xif1, column_id="id")
    return extracted_features
    

def tffeature():
    from tsfresh.examples.robot_execution_failures import download_robot_execution_failures, \
        load_robot_execution_failures

    Xi, Yi, df_target = readinput()
    Xi = Xi.transpose()


    dff = pd.DataFrame()


    # index = 0
    # for i in range(0, Xi.shape[1]*0.25):
    #     Xif1 = Xi.iloc[:, i]
    #
    #     Xif1 = pd.DataFrame(Xif1)
    #     Xif1["id"] = index
    #     index = index + 1
    #     from tsfresh import extract_features
    #     extracted_features = extract_features(Xif1, column_id="id")
    #     dff = dff.append(extracted_features)
    # dff.to_csv("1121.csv")

    Xi = df_target
    index = 1
    for i in range(0, Xi.shape[1]):
        Xif1 = Xi.iloc[:, i]

        Xif1 = pd.DataFrame(Xif1)
        Xif1["id"] = index
        # index = index
        from tsfresh import extract_features
        extracted_features1 = extract_features(Xif1, column_id="id")
        # dff = dff.append(extracted_features1)
        if i ==0:
            dff = dff.append(extracted_features1)
        else:
            dff = pd.DataFrame(np.concatenate([dff.to_numpy(),extracted_features1.to_numpy()]),columns=extracted_features1.columns)
    dff.to_csv("Target_features1000.csv")



    index = 0
    for i in range(0, int(Xi.shape[1]*0.25)):
        Xif1 = Xi.iloc[:, i]
    
        Xif1 = pd.DataFrame(Xif1)
        Xif1["id"] = index
        index = index + 1
        from tsfresh import extract_features
        extracted_features = extract_features(Xif1, column_id="id")
        dff = dff.append(extracted_features)
    dff.to_csv("1121_1.csv")

    # index = 0
    # for i in range(int(Xi.shape[1]*0.25)+1, int(Xi.shape[1]*0.5)):
    #     Xif1 = Xi.iloc[:, i]
    #
    #     Xif1 = pd.DataFrame(Xif1)
    #     Xif1["id"] = index
    #     index = index + 1
    #     from tsfresh import extract_features
    #     extracted_features = extract_features(Xif1, column_id="id")
    #     dff = dff.append(extracted_features)
    # dff.to_csv("1121_2.csv")
    #
    # index = 0
    # for i in range(int(Xi.shape[1]*0.5)+1, int(Xi.shape[1]*0.75)):
    #     Xif1 = Xi.iloc[:, i]
    #
    #     Xif1 = pd.DataFrame(Xif1)
    #     Xif1["id"] = index
    #     index = index + 1
    #     from tsfresh import extract_features
    #     extracted_features = extract_features(Xif1, column_id="id")
    #     dff = dff.append(extracted_features)
    # dff.to_csv("1121_3.csv")
    #
    # index = 0
    # for i in range(int(Xi.shape[1]*0.75)+1, Xi.shape[1]):
    #     Xif1 = Xi.iloc[:, i]
    #
    #     Xif1 = pd.DataFrame(Xif1)
    #     Xif1["id"] = index
    #     index = index + 1
    #     from tsfresh import extract_features
    #     extracted_features = extract_features(Xif1, column_id="id")
    #     dff = dff.append(extracted_features)
    # dff.to_csv("1121_4.csv")




    from tsfresh import extract_features
    extracted_features = extract_features(Xif1, column_id="id")
    extracted_features2 = extract_features(Xif2, column_id="id")
    extracted_features3 = extract_features(Xif3, column_id="id")



    extracted_features = extract_features(timeseries, column_id="id", column_sort="time")
    print("")
    extracted_features.to_csv("111.csv")
    pd.concat([extracted_features,extracted_features2,extracted_features3]).to_csv("1000plus.csv")


    from tsfresh import select_features
    from tsfresh.utilities.dataframe_functions import impute
    extracted_features = dff
    impute(extracted_features)
    # Y = Yi.iloc[0:10,0]
    features_filtered = select_features(extracted_features, Yi.to_numpy())
    features_filtered.to_csv("test_part.csv")

    from tsfresh import extract_relevant_featuresd
    
    features_filtered_direct = extract_relevant_features(timeseries, Yi,
                                                         column_id='id')

    print(features_filtered_direct)

if __name__ == '__main__':
    tffeature()
